# Configuration des environnements DOCKER

## Mise en place d'un nouveau projet
- mkdir nom_du_projet
- cd nom_du_projet
- git clone [ssh:Core/docker] .
- copier le ficher .env.bak sur .env
- modifier le contenu du fichier .env
- créer une branche correspondant au nom_du_projet
- push la branche sur le répo distant
- docker-compose build
- docker-compose up -d
- cd app/
- git clone [ssh:Core/symfony4] .
- copier le fichier .env sur .env.local
- modifer le contenu du fichier .env.local
- cd ..
- les commandes suivantes peuvent nécéssité le préfix "winpty" suivant le terminal utilisé
  - docker-compose exec php composer install
  - docker-compose exec node yarn install
  - docker-compose exec node yarn encore dev
- se connecter sur http://localhost:[port_container_NGINX]
- git remote rename origin old-origin
- git remote add origin [ssh:REPO_DU_PROJET]
- git push -u origin --all

## déploiement local d'un projet existant
- mkdir nom_du_projet
- cd nom_du_projet
- git clone [ssh:Core/docker] .
- git checkout sur la branche du projet à déployer
- copier le ficher .env.bak sur .env
- modifier le contenu du fichier .env
- docker-compose build
- docker-compose up -d
- cd app/
- git clone [ssh:REPO_DU_PROJET] .
- copier le fichier .env sur .env.local
- cd ..
- les commandes suivantes peuvent nécéssité le préfix "winpty" suivant le terminal utilisé
  - docker-compose exec php composer install
  - docker-compose exec node yarn install
  - docker-compose exec node yarn encore dev
- se connecter sur http://localhost:[port_container_NGINX]

## lancer l'environnement DOCKER
> S'assurer que les ports soient disponibles pour le lancement de l'environnement

- en étant dans le répertoire racine contenant le ficher "docker-compose.yml"
- docker-compose build (à l'initialisation)
- docker-compose up -d

## exécuter les commandes sur les sources
- uniquement possible lorsque l'environnement DOCKER est lancé
- docker-compose exec [nom_du_container] [commande à exécuter sur les sources]